<?php
  include 'partials/header.php';
 ?>
<link rel="stylesheet" href="css/about.css">
<h1 id="aboutus"> ABOUT US </h1>
<div id="content">
  <div class="content1"><h3 class="info">KIT STORE është dyqan online me porosi, të gjitha porositë bëhen direkt në fabrikë në Tajlandë dhe dorëzohen tek klientët tanë anë e mbanë botës.<br><br>
Jemi gjithashtu përfaqësues zyrtar ADIDAS .</h3></div>
  <div class="content1"><h3 class="info">KIT STORE is an online store that delivers orders in all over the world. All the orders are made directly to our factory in Thailand and gets delivered to our clients by post.<br><br>
We are also official distributors of ADIDAS.</h3></div>
  <div class="content2">

    <h2  class="infob"> Info rreth blerjeve</h2>
    <h3 class="infoc"> Çfarë metodash transporti kemi? </h3>
    <p class="info2">KIT STORE mundëson transport në të gjithë botën FALAS me Standard Economy Shipping. Për forma më të shpejta të transportit na kontaktoni në email : kitstore@gmail.com</p>
    <h3 class="infoc"> Sa kohë zgjat transporti? </h3>
    <p class="info2">Të gjitha produktet porositen në fabrikë në Tajlandë dhe vijnë me postë direkt në adresen tuaj për zakonisht 2-3 javë, varet nga postat. Në USA dhe disa shtete të Europës zakonisht shkojnë edhe më shpejt.</p>

  </div>
  <div class="content2"><h2  class="infob"> Shopping Informations</h2>
  <h3 class="infoc"> What shipping methods are available? </h3>
  <p class="info2">KIT STORE can ship internationally for FREE using Standard Economy Shipping method. If you want faster shipping feel free to contact us by email : kitstore@gmail.com</p>
  <h3 class="infoc"> How many time it needs to be delivered? </h3>
  <p class="info2">All the products ordered here are shipped directly from our factory located in Thailand and all the products come from Thailand for usually 2-3 weeks, it depends on international postal services. On US and some other European countries the shipping is usually faster.</p></div>



  <div class="content2">
    <h2  class="infob"> Pagesat</h2>
    <h3 class="infoc"> Cilat janë metodat e pagesave? </h3>
    <p class="info2">Metodat e pagesave janë :</p>
    <ul>
    <li> Pagesa KESH </li>
    <li> Raiffeisen Bank </li>
    <li> TEB Bank </li>
    <li> Western Union/Money Gram/RIA </li>
    <li> Pay Pal </li>
  </ul>
  </div>
  <div class="content2">


    <h2  class="infob"> Payments</h2>
    <h3 class="infoc">What Payment Methods Are Accepted? </h3>
    <p class="info2">Payment methods are :  </p>
    <ul>
    <li> Cash payment  </li>
    <li> Raiffeisen Bank </li>
    <li> TEB Bank </li>
    <li> Western Union/Money Gram/RIA </li>
    <li> Pay Pal </li>
  </ul>
  </div>
  <div class="content3">
    <h2  class="infob"> Porositë dhe kthimet</h2>
    <h3 class="infoc">Si të bëj një porosi? </h3>
    <p class="info2">Për të bërë porosi kliko në produktin që dëshiron, zgjedh madhësinë, ngjyrën ose modelin dhe kliko “Add to Cart”. Pastaj kliko “Checkout” dhe plotëso fushat rreth adresës tënde. Zgjedh metodën e pagesave dhe përfundo porosinë.</p>
    <h3 class="infoc"> Si mund ta gjurmoj porosinë time? </h3>
    <p class="info2">Për ta gjurmuar porosinë tuaj, kliko tek menyja “Gjurmimi i porosisë”, pastaj shkruaje numrin e porosisë dhe email adresën dhe kliko TRACK.</p>

  </div>
  <div class="content3">

    <h2  class="infob"> Orders and Returns</h2>
    <h3 class="infoc">How Can I make an order?</h3>
    <p class="info2">To make an order you first should click to the product that you like to order.Then choose the size,color or the model and click “Add to cart”. After that click “Checkout” then fill the areas that asks for your address. Choose the payment form then finish the order.</p>
    <h3 class="infoc"> How Do I Track My Order?</h3>
    <p class="info2">To track your order you should click at the “Gjurmimi i porosisë” on the menu, then fill the areas (Order number and email) then click TRACK.</p></div>
  </div>
</div>

<?php include 'partials/footer.php' ?>
