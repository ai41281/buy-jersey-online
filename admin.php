<?php include 'partials/header.php'; ?>

<?php
if($_SESSION['is_admin'] == '1'){
    echo "<a href='admin.php'></a>";
}

else{
  header('Location: index.php');
}

?>
<?php  include 'dbconnect.php'?>
<?php
    $msg="";
    if(isset($_POST['upload'])){
    isset($_POST['ekip']) ? $isEkip = 1 : $isEkip = 0;
    $target = "images/".basename($_FILES['image']['name']);
    $image = $_FILES['image']['name'];
    $price=$_POST['price'];
    $emertimi=$_POST['emertimi'];
    $emriekipit=$_POST['emriekipit'];

    $query = $pdo->prepare("INSERT INTO produktet (image, price,emertimi,ekip,emriekipit) VALUES (:image, :price, UPPER(:emertimi), :ekip,:emriekipit)");
    $query->bindParam(':image', $image);
    $query->bindParam(':price', $price);
    $query->bindParam(':emertimi', $emertimi);
    $query->bindParam(':ekip', $isEkip);
    $query->bindParam(':emriekipit', $emriekipit);
    $query->execute();
    if(move_uploaded_file($_FILES['image']['tmp_name'],$target)){
    $msg="Image Uploaded";
  }
    else{
   $msg= "Error";
  }
  } ?>

<link rel="stylesheet" href="css/admin.css">

<script src="js/main.js"> </script>
<div id="panel">
  <h1 id="adminpanel"> ADMIN PANEL <h1>
  <h1 id="welcome">Welcome <?php echo $_SESSION['name']; ?></h1>

</div>

<div id="opsionet">
<button onclick="myFunction()" class="opt">➕ Shto Produkt</button>
<button onclick="fshij()" class="opt">➖ 🗑️ Edito Produkt</button>
<button onclick="showusers()" class="opt">👦  USERS </button>
<button onclick="showemails()" class="opt">📬  EMAILS </button>
<button onclick="showorders()" class="opt">📦  ORDERS </button>
 </div>
<div id="myDIV">

  <form method="POST" action="" enctype="multipart/form-data">
    <input class="details"type="text"  name="emertimi" placeholder="Emertimi" required>
    <input type="number"  name="price" placeholder="Shkruaj Çmimin" required>
    <br>
      <label> EKIP </label>
      <input type="checkbox" name="ekip" >
      <br>
      <input type="file" name="image" required/>
      <br>
      <br>
      <select name="emriekipit" >
      <option value="KOSOVA">KOSOVA</option>
      <option value="ALBANIA">ALBANIA</option>
      <option value="GERMANY">GERMANY</option>
      <option value="ENGLAND">ENGLAND</option>
      <option value="SPAIN">SPAIN</option>
      <option value="NETHERLANDS">NETHERLANDS</option>
      <option value="ARGENTINA">ARGENTINA</option>
      <option value="PORTUGAL">PORTUGAL</option>
      <option value="CROATIA">CROATIA</option>
      <option value="BELGIUM">BELGIUM</option>
      <option value="BRAZIL">BRAZIL</option>
      <option value="SWITZERLAND">SWITZERLAND</option>
      <option value="URUGUAY">URUGUAY</option>
      <option value="ITALY">ITALY</option>
      <option value="BARCELONA"> BARCELONA </option>
      <option value="REALMADRID"> REAL MADRID</option>
      <option value="ATLETICOMADRID"> ATLETICO MADRID</option>
      <option value="JUVENTUS"> JUVENTUS</option>
      <option value="MILANO"> MILANO</option>
      <option value="INTER"> INTER</option>
      <option value="ROMA"> ROMA</option>
      <option value="BAYERNMUNCHEN"> BAYERN MUNCHEN</option>
      <option value="LIVERPOOL">LIVERPOOL</option>
      <option value="MANUNITED">MAN UNITED</option>
      <option value="MANCITY">MAN CITY</option>
      <option value="CHELSEA">CHELSEA</option>
      <option value="ARSENAL">ARSENAL</option>
      <option value="PSG">PSG</option>
      <option value="TJETER">TJETER...</option>
    </select>
    <br>
    <input type="submit" name="upload" value="SHTO PRODUKTIN">
  </form>
</div>

<div id="fshij">
  <?php
  $sql = $pdo->prepare("SELECT * FROM produktet order by id desc " );
  $sql->execute();
  while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

    echo "<div id='prod'>";
    echo "<img src='images/".$row['image']."' >";
    echo"<br>";
    echo '<h4 class="prodname">'.$row['emertimi'].'</h4> ';
    echo "<p class='prodname'>".'€'.$row['price'].".00</p>";
    echo '<a class="prodname buton" href="delete.php?id='.$row['id'].'">FSHIJE</a> ';
    echo '<a class="prodname buton" href="editproduct.php?id='.$row['id'].'">EDIT</a> ';
    echo"</div>";
  }
  ?>
</div>


<div id="users">
  <?php
  $query = $pdo->query('SELECT * from users');
  $users = $query->fetchAll();
  ?>
  <table>
      <thead>
          <tr>
              <th>Emri</th>
              <th>Email</th>
              <th>Opsioni</th>
          </tr>
      </thead>
      <tbody>
          <?php foreach($users as $user): ?>
              <tr>
                  <td><h4><?php echo $user['name']; ?><h4></td>
                  <td><h4><?php echo $user['email']; ?><h4></td>
                  <td><h4><a href="delete-user.php?id=<?php echo $user['id']; ?>">Delete</a><h4></td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
</div>



<div id="emails">
  <?php
  $sql = $pdo->prepare("SELECT * FROM emails order by data DESC" );
  $sql->execute();
  echo"<table>";
  echo"<th>";
  echo"<h3> Emri </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Mbiemri </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Emaili </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Mesazhi </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Data </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Fshije </h3>";
  echo"</th>";
  while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

    echo"<tr>";
    echo"<td>";
  echo"<h3> ".$row['name']." </h1> ";
  echo"</td>";
    echo"<td>";
  echo"<h3> ".$row['surname']." </h1> ";
  echo"</td>";
  echo"<td>";
  echo"<h3> ".$row['email']." </h1> ";
  echo"</td>";
  echo"<td id='teksti'>";
  echo"<h3 id='teksti'> ".$row['text']." </h1> ";
  echo"</td>";
  echo"<td>";
  echo"<h3> ".$row['data']." </h1> ";
  echo"</td>";
  echo"<td>";
  echo"<h4><a href='deleteemail.php?id=".$row['id']."'>🗑️</a></h4>";
  echo"</td>";
  echo"</tr>";

}
echo"</table>";
  ?>
</div>




<div id="orders">
  <?php
  $sql = $pdo->prepare("SELECT * FROM porosit group by orderid order by data DESC" );
  $sql->execute();
  echo"<table>";
  echo"<th>";
  echo"<h3> Order ID </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Totali </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Adresa </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Email </h3>";
  echo"</th>";
  echo"<th>";
  echo"<h3> Data </h3>";
  echo"</th>";


  while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {

    echo"<tr>";
    echo"<td>";
    echo"<h3><a href='porosia.php?orderid=".$row['orderid']." '> #".$row['orderid']." </a><h3><br>";
    echo"</td>";
    echo" <td >";
    $show  =$pdo->prepare( "SELECT SUM(totali) AS totalsum FROM porosit where orderid=".$row['orderid']."");
    $show->execute();
    while($res = $show->fetch(PDO::FETCH_ASSOC)):
      echo" <h3  class='details'>Totali: ".$res['totalsum'].".00€</h3> ";
    endwhile;
    echo" </td>";
    echo"<td>";
    echo"<h3> ".$row['adresa']." </h1> ";
    echo"</td>";
    echo"<td>";
    echo"<h3> ".$row['email']." </h1> ";
    echo"</td>";
  echo"<td>";
  echo"<h3> ".$row['data']." </h1> ";
  echo"</td>";
  echo"</tr>";

}
echo" <td class='tydy' colspan='6'>";
$show  =$pdo->prepare( "SELECT SUM(totali) AS totalsum FROM porosit");
$show->execute();
while($res = $show->fetch(PDO::FETCH_ASSOC)):
  echo" <h3  class='details'>Totali: ".$res['totalsum'].".00€</h3> ";
endwhile;
echo" </td>";
echo"</table>";
  ?>
</div>
