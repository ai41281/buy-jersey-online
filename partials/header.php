<?php session_start(); ?>

<header>
    <link rel="stylesheet" href="css/style.css">
    <img id="logo" src="img/logo.png">
    <h3 id="title">BUY KIT ONLINE</h3>
    <ul id="menyja">
      <li><a href="index.php">HOME</a></li>
      <li><a href="produktet.php">PRODUKTET</a></li>
      <li><a href="about.php">PËR NE</a></li>
      <li><a href="search.php">🔎</a></li>
            <?php if(isset($_SESSION['name']) && $_SESSION['name'] && $_SESSION['is_admin']==0): ?>
      <li><a href="kontakt.php?id=<?php echo $_SESSION['id']; ?>">📝</a></li>
      <?php endif;?>
      <?php if(isset($_SESSION['name']) && $_SESSION['name']): ?>
      <li><a href="cart.php">🛒</a></li>
      <li><a href="orders.php">📦</a></li>
    <?php endif;?>
    <?php if(isset($_SESSION['is_admin']) && $_SESSION['is_admin']==1): ?>
    <li><a href="admin.php">🔧</a></li>
  <?php endif;?>
  <?php if(isset($_SESSION['name']) && $_SESSION['name']): ?>
    <li><a href="profili.php">👦</a></li>
  <?php endif; ?>
      <?php if(isset($_SESSION['name']) && $_SESSION['name']): ?>
                  <li><a href="logout.php"><img src="img/logout2.png"></a></li>
              <?php else: ?>
                  <li><a href="login.php"><img src="img/login.png"></a></li>
                      <?php endif; ?>
                    </ul>
</header>
