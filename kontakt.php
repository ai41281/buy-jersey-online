        <?php include 'partials/header.php'; ?>
<?php
    include 'dbconnect.php';

            if(!isset($_SESSION['id'])) {
            header('Location: ./index.php');
        }

    if(isset($_POST['submitted'])) {
        $query = $pdo->prepare("INSERT INTO emails (name,surname,email,text) values (:name,:surname,:email,:text)");
        $query->bindParam(':name', $_POST['name']);
        $query->bindParam(':surname', $_POST['surname']);
        $query->bindParam(':email', $_POST['email']);
        $query->bindParam(':text', $_POST['text']);
        $query->execute();
        header("Location: ./kontakt.php");
    }
?>





        <title> KONTAKT </title>
         <link rel="stylesheet" type="text/css" href="css/contacts.css">

        <div class="kat">
            <div id="contact-form">
            <h3 id="forma">Forma për kontakt</h3>
            <form action="kontakt.php" method="POST">
                <label for="fname">Emri</label>
                <input readonly='readonly'type="text" id="fname" name="name" value="<?php echo $_SESSION['name']; ?>" placeholder="Emri i juaj">
                <input readonly='readonly' type="text" id="fname" name="surname" value="<?php echo $_SESSION['surname']; ?>" placeholder="Mbiemri i juaj">
                <input id="email" readonly='readonly' type="email" id="fname" name="email" value="<?php echo $_SESSION['email']; ?>" placeholder="Email">

                <br>
                <label for="subject">Mesazhi</label>
                <textarea id="subject" name="text" placeholder="Shëno diçka" maxlength="500";></textarea>
                <input type="submit" name="submitted" value="Konfirmo">
            </form>
            </div>
        </div>
        <?php include 'partials/footer.php' ?>
