<?php

    include 'dbconnect.php';

    $id = $_GET['id'];

    $query = $pdo->prepare('DELETE FROM emails WHERE id = :id');
    $query->bindParam('id', $id);

    $query->execute();

    header('Location: admin.php');
?>
