
<?php include 'dbconnect.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
      <?php  include 'partials/header.php' ?>
      <section class="cd-slider">
        <ul>
        <li data-color="#FF384B">
          <div class="content" style="background-image:url(img/barcelona19.png)">
            <blockquote>
              <p>Fanelat më të reja</p>
            </blockquote>
          </div>
        </li>
        <li data-color="#FF9C00">
          <div class="content" style="background-image:url(img/liverpool19.png)">
            <blockquote>
              <p>Kualiteti më i mirë</p>
            </blockquote>
          </div>
        </li>
      </ul>
      <nav>
        <div><a class="prev" href="#"></a></div>
        <div><a class="next" href="#"></a></div>
      </nav>
    </section>

    <div id="firmat">
      <img class="firma"src="img/adidas2.png">
      <img class="firma" src="img/nike2.png">
      <img class="firma" src="img/puma2.png">
      <img class="firma" src="img/newbalance2.png">
    </div>

    <h1 id="kategorite"> Kategoritë e fanelave </h1>
    <div id="ekipkomb">
      <a href="ekip.php"><div class="box"><h3 class="kategoria"> EKIP </h3><img class="ekipi" src="img/ajax19.png"></div></a>
      <a href="kombetare.php"><div class="box"><h3 class="kategoria"> KOMBËTARE </h3><img class="ekipi" src="img/kosova.png"> </div></a>
    </div>

    <div id="content1">
      <h1 id="new"> Produktet më të reja </h1>
      <?php
      $sql = $pdo->prepare("SELECT * FROM produktet ORDER BY id DESC LIMIT 4" );
      $sql->execute();
      while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
        echo "<div id='img_div'>";
        echo "<img src='images/".$row['image']."' > <br>";
        echo '<a href="produkti.php?id='.$row['id'].'&emriekipit='.$row['emriekipit'].'">'.$row['emertimi'].'</a> ';
        echo "<p>".'€'.$row['price'].".00</p>";
        echo "</div>";
      }
      ?>

    </div>
        <br>
        <br>
    <?php  include 'partials/footer.php' ?>
    <script>

if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}

(function() {

  var autoUpdate = true,
      timeTrans = 4000;

	var cdSlider = document.querySelector('.cd-slider'),
		item = cdSlider.querySelectorAll("li"),
		nav = cdSlider.querySelector("nav");

	item[0].className = "current_slide";

	for (var i = 0, len = item.length; i < len; i++) {
		var color = item[i].getAttribute("data-color");
		item[i].style.backgroundColor=color;
	}

	var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE");
		if ( msie > 0 ) {
			var version = parseInt(ua.substring(msie+ 5, ua.indexOf(".", msie)));
			if (version === 9) { cdSlider.className = "cd-slider ie9";}
	}

	if (item.length <= 1) {
		nav.style.display = "none";
	}

	function prevSlide() {
		var currentSlide = cdSlider.querySelector("li.current_slide"),
			prevElement = currentSlide.previousElementSibling,
			prevSlide = ( prevElement !== null) ? prevElement : item[item.length-1],
			prevColor = prevSlide.getAttribute("data-color"),
			el = document.createElement('span');

		currentSlide.className = "";
		prevSlide.className = "current_slide";

		nav.children[0].appendChild(el);

		var size = ( cdSlider.clientWidth >= cdSlider.clientHeight ) ? cdSlider.clientWidth*2 : cdSlider.clientHeight*2,
		    ripple = nav.children[0].querySelector("span");

		ripple.style.height = size + 'px';
		ripple.style.width = size + 'px';
		ripple.style.backgroundColor = prevColor;

		ripple.addEventListener("webkitTransitionEnd", function() {
			if (this.parentNode) {
				this.parentNode.removeChild(this);
			}
		});

		ripple.addEventListener("transitionend", function() {
			if (this.parentNode) {
				this.parentNode.removeChild(this);
			}
		});

	}

	function nextSlide() {
		var currentSlide = cdSlider.querySelector("li.current_slide"),
			nextElement = currentSlide.nextElementSibling,
			nextSlide = ( nextElement !== null ) ? nextElement : item[0],
			nextColor = nextSlide.getAttribute("data-color"),
			el = document.createElement('span');

		currentSlide.className = "";
		nextSlide.className = "current_slide";

		nav.children[1].appendChild(el);

		var size = ( cdSlider.clientWidth >= cdSlider.clientHeight ) ? cdSlider.clientWidth*2 : cdSlider.clientHeight*2,
			  ripple = nav.children[1].querySelector("span");

		ripple.style.height = size + 'px';
		ripple.style.width = size + 'px';
		ripple.style.backgroundColor = nextColor;

		ripple.addEventListener("webkitTransitionEnd", function() {
			if (this.parentNode) {
				this.parentNode.removeChild(this);
			}
		});

		ripple.addEventListener("transitionend", function() {
			if (this.parentNode) {
				this.parentNode.removeChild(this);
			}
		});

	}

	updateNavColor();

	function updateNavColor () {
		var currentSlide = cdSlider.querySelector("li.current_slide");

		var nextColor = ( currentSlide.nextElementSibling !== null ) ? currentSlide.nextElementSibling.getAttribute("data-color") : item[0].getAttribute("data-color");
		var	prevColor = ( currentSlide.previousElementSibling !== null ) ? currentSlide.previousElementSibling.getAttribute("data-color") : item[item.length-1].getAttribute("data-color");

		if (item.length > 2) {
			nav.querySelector(".prev").style.backgroundColor = prevColor;
			nav.querySelector(".next").style.backgroundColor = nextColor;
		}
	}

	nav.querySelector(".next").addEventListener('click', function(event) {
		event.preventDefault();
		nextSlide();
		updateNavColor();
	});

	nav.querySelector(".prev").addEventListener("click", function(event) {
		event.preventDefault();
		prevSlide();
		updateNavColor();
	});

  setInterval(function() {
    if (autoUpdate) {
      nextSlide();
      updateNavColor();
    };
	},timeTrans);

})();

    </script>

  </body>
</html>
