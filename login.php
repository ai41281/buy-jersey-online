<?php include 'partials/header.php'; ?>
<?php
        if(isset($_SESSION['id'])) {
        header('Location: ./index.php');
    }
?>
<?php include 'dbconnect.php'; ?>
<?php
    if(isset($_POST['submitted'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $query = $pdo->prepare('SELECT * FROM users WHERE email = :email');
        $query->bindParam(':email', $email);
        $query->execute();

        $user = $query->fetch();

        if(is_array($user)&&count($user) > 0 && password_verify($password, $user['password'])) {
            $_SESSION['id'] = $user['id'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['surname'] = $user['surname'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['adresa'] = $user['adresa'];
            $_SESSION['is_admin'] = $user['is_admin'];
            $roli=$user['is_admin'];
            if($roli==='1'){
                  header('Location: ./admin.php');
            }
            elseif($roli==='0'){  header('Location: ./index.php');}
        }else {
              echo "<h4 id='gabim'> Wrong email or password </h4> ";
        }
    }
?>

<title>LOG IN</title>
    <link rel="stylesheet" href="css/login.css">
    <div class="container">
    <img id="banner"src="img/bannerls.png">
      <div id="katrori">
        <div id="form">
        <form action="" method="POST">
            <label for="email">Email Address</label>
            <input id="email" type="email" placeholder="Enter your email" name="email" required><br>
            <label for="password">Password</label>
            <input type="password" placeholder="Enter your password" name="password" required><br>
            <input type="submit" name="submitted" value="LOG IN">
        </form>
        <br>
        <br>
        <br>
        <br>
        <a id="signup"href="signup.php">SIGN UP </a>
            </div>
          </div>
  </div>
